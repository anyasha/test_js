 // У вас есть массив строк, каждая строка - один продукт, который входит в состав английского завтрака. Напишите код, который работает так: если в состав завтрака входит больше 4х продуктов, то остальные переносятся на обед.



let englishBreakfast = ["поджареный бекон", "колбаски", "яичница", "жареные грибы", "жареные помидоры", "фасоль", "хлеб с джемом", "кофе"];

/* в результате массив englishBreakfast должен быть равен:
englishBreakfast = ["поджареный бекон", "колбаски", "яичница", "жареные грибы"];

и должен появится еще массив dinner, со значениями:
dinner = ["жареные помидоры", "фасоль", "хлеб с джемом", "кофе"];
*/
if (englishBreakfast.length <= 4) {
  console.log(`English breakfast is ${englishBreakfast}`);
} else {
 const  dinner = englishBreakfast.slice(3, );
 englishBreakfast = englishBreakfast.slice(0,3);
  console.log(`English breakfast is ${englishBreakfast}`);
  console.log(`English dinner is ${dinner}`);
}
